<?php ini_set("display_errors",0);
$student_id = $_REQUEST['studentid'];
$center_id = $_REQUEST['centerid']; 
$course_id = $_REQUEST['courseid'];
$enroll_amount = $_REQUEST['enrollamount'];
$payment_type = $_REQUEST['paymenttype']; 
$payment_status = $_REQUEST['paymentstatus'];
$valid_from = $_REQUEST['validfrom']; 

$response = array();

header("content-type:application/json");

if(strlen($student_id) != 0 && strlen($center_id) != 0){	
	include("../function.php");			 
	$response = StudentEnrollment($student_id,$center_id,$course_id,$enroll_amount,$payment_type,$payment_status,$valid_from);
	 
}else{
	$tmp = array();
	$tmp['Status']="False";
	$tmp['Message']="Parameter Not found";
	array_push($response, $tmp);
}
echo json_encode($response);
?>