<?php ini_set("display_errors",0);
$user_id = $_REQUEST['user_id'];
$course_id = $_REQUEST['course_id'];
$batch_id = $_REQUEST['batch_id'];  
$response = array();

header("content-type:application/json");

if(strlen($user_id) != 0 && strlen($course_id) != 0 && strlen($batch_id) != 0){	
	include("../function.php");			 
	$response = LinkCourse($user_id,$course_id,$batch_id);
	 
}else{
	$tmp = array();
	$tmp['Status']="False";
	$tmp['Message']="Parameter Not found";
	array_push($response, $tmp);
}
echo json_encode($response);
?>